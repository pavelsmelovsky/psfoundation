//
//  TestDataHex.swift
//  PSFoundationTests
//
//  Created by Pavel Smelovsky on 22.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest
@testable import PSFoundation

let hex = "321ACF4B95DE321ACF4B95DE321ACF4B95DE321ACF4B95DE"

class TestDataHex: XCTestCase {
    var data: Data!
    var bigData: Data!

    override func setUp() {
        super.setUp()

        guard let nsData = NSData(fromHex: hex) else { fatalError() }
        self.data = nsData as Data

        if self.bigData == nil {
            var bigHex = ""
            for _ in 0 ..< 10_000_000 {
                bigHex += hex
            }
            guard let nsBigData = NSData(fromHex: bigHex) else { fatalError() }
            self.bigData = nsBigData as Data
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConversion() {
        let string = self.data.hex
        XCTAssertEqual(string, hex)

        let nsstring = (self.data as NSData).hexString()
        XCTAssertEqual(nsstring, hex)
    }

    func testPerformanceSwift() {
        // This is an example of a performance test case.
        self.measure {
            let str = self.data.hex
            XCTAssertEqual(str, hex)
        }
    }

    func testPerformanceC() {
        // This is an example of a performance test case.
        self.measure {
            let nsdata = self.data as NSData
            let str = nsdata.hexString()
            XCTAssertEqual(str, hex)
        }
    }
}
