//
//  TestStringEmail.swift
//  PSFoundationTests
//
//  Created by Pavel Smelovsky on 27.01.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import XCTest
@testable import PSFoundation

class TestStringEmail: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testEmails() {
        XCTAssertTrue("pavel@email.com".isEmail)
        XCTAssertFalse("p.s.@email.com".isEmail)
        XCTAssertFalse("pavel@email".isEmail)
        XCTAssertFalse("pavel@.com".isEmail)
        XCTAssertFalse("pavel.email.com".isEmail)
        XCTAssertFalse("pavel@".isEmail)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
