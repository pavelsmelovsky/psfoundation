//
//  SPFileUploader.h
//  SPFoundation
//
//  Created by Pavel Smelovsky on 23.03.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SPFileUploaderCompletion)(NSData* responseData, NSError* error);


@interface SPFileUploader : NSObject

+ (instancetype)uploadSourceURL:(NSURL *)sourceURL toDestinationURL:(NSURL *)destinationURL progress:(NSProgress *)progress completion:(SPFileUploaderCompletion)completion;


@property (nonatomic, readonly, copy) NSURL* sourceURL;
@property (nonatomic, readonly, copy) NSURL* destinationURL;

@property (nonatomic, copy) SPFileUploaderCompletion completion;

@property (nonatomic, readonly, strong) NSProgress* progress;



- (instancetype)initWithSourceURL:(NSURL *)sourceURL andDestinationURL:(NSURL *)destinationURL progress:(NSProgress *)progress;

- (void)upload;

@end
