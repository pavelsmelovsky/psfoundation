//
//  SPFileUploader.m
//  SPFoundation
//
//  Created by Pavel Smelovsky on 23.03.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import "SPFileUploader.h"

@interface SPFileUploader() <NSURLSessionTaskDelegate>

@property (nonatomic, strong) NSURLSession* session;
@property (nonatomic, strong) NSURLSessionTask* task;

@property (nonatomic, strong) NSMutableData* receivedData;

@end



@implementation SPFileUploader

+ (instancetype)uploadSourceURL:(NSURL *)sourceURL toDestinationURL:(NSURL *)destinationURL progress:(NSProgress *)progress completion:(SPFileUploaderCompletion)completion
{
    SPFileUploader* fileUploader = [[SPFileUploader alloc] initWithSourceURL:sourceURL andDestinationURL:destinationURL progress:progress];
    fileUploader.completion = completion;
    [fileUploader upload];
    return fileUploader;
}

- (instancetype)initWithSourceURL:(NSURL *)sourceURL andDestinationURL:(NSURL *)destinationURL progress:(NSProgress *)progress
{
    if (self = [super init])
    {
        _sourceURL = sourceURL;
        _destinationURL = destinationURL;
        _progress = progress;

        self.session = [self __configureSession];
    }
    return self;
}

- (NSURLSession *)__configureSession
{
    NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];

    NSURLSession* session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue new]];

    return session;
}

- (void)upload
{
    _progress = [NSProgress progressWithTotalUnitCount:100];

    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:self.destinationURL];
    [request setHTTPMethod:@"POST"];
    NSURLSessionTask* task = [self.session uploadTaskWithRequest:request fromFile:self.sourceURL];
    [task resume];
}




#pragma mark - URLSession delegate

/* Sent periodically to notify the delegate of upload progress.  This
 * information is also available as properties of the task.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{

}

/* Sent as the last message related to a specific task.  Error may be
 * nil, which implies that no error occurred and this task is complete.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if (self.completion) {
        self.completion(self.receivedData, error);
    }
}




#pragma mark - URLSession Data delegate


/* Sent when data is available for the delegate to consume.  It is
 * assumed that the delegate will retain and not copy the data.  As
 * the data may be discontiguous, you should use
 * [NSData enumerateByteRangesUsingBlock:] to access it.
 */
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    if (!self.receivedData)
    {
        self.receivedData = [NSMutableData data];

        if (self.progress && self.progress.totalUnitCount == 0) {
            self.progress.totalUnitCount = dataTask.countOfBytesExpectedToSend;
        }
    }
    [self.receivedData appendData:data];

    if (self.progress) {
        self.progress.completedUnitCount += data.length;
    }
}

@end
