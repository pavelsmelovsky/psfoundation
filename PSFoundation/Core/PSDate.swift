//
//  PSDate.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 19.11.14.
//  Copyright (c) 2014 Pavel Smelovsky. All rights reserved.
//

#if os(OSX)
    import AppKit
#elseif os(iOS)
    import UIKit
#endif

private var dateTimeFormatString = "yyyy-MM-dd HH:mm:ss"

private var _initialized = false

private var formatters = [String: DateFormatter](minimumCapacity: 10)

@objc(PSDate)
open class PSDate: NSObject
{
    public static func initialize(dateFormat: String?) {
        if let dateFormat = dateFormat {
            dateTimeFormatString = dateFormat
        }
        _initialized = true
    }

    @available(*, deprecated)
    class var dateFormatter: DateFormatter {
        assert(_initialized, "Need to initialize SPDate public classbefore using it")

        return dateFormatter(format: dateTimeFormatString, timeZone: nil)
    }

    public static func dateFormatter(format: String, timeZone: TimeZone?) -> DateFormatter {
        var formatter = formatters[format]
        if formatter == nil {
            formatter = DateFormatter()
            formatter!.calendar = Calendar.current
            formatter!.dateFormat = format
            formatter!.locale = Locale.current

            formatters[format] = formatter!
        }

        formatter?.timeZone = timeZone ?? TimeZone.autoupdatingCurrent

        return formatter!
    }

    public static func parse(_ value: String) -> Date? {
        let formatter = self.dateFormatter(format: dateTimeFormatString, timeZone: nil)
        return formatter.date(from: value)
    }

    public static func parse(_ value: String, format: String) -> Date? {
        let formatter = self.dateFormatter(format: format, timeZone: nil)
        return formatter.date(from: value)
    }

    public static func parse(_ value: String, timeZone: TimeZone) -> Date? {
        let formatter = self.dateFormatter(format: dateTimeFormatString, timeZone: timeZone)
        return formatter.date(from: value)
    }

    public static func parse(_ value: String, format: String, timeZone: TimeZone) -> Date? {
        let formatter = self.dateFormatter(format: format, timeZone: timeZone)
        return formatter.date(from: value)
    }

    /**
     Parse only DATE from string
     
     - parameter value: string representation of the date
     
     - returns: NSDate object if date is parsed successfully, otherwise nil
     */
    public static func parseDate(_ value: String) -> Date? {
        let units: Set<Calendar.Component> = [.year, .month, .day]

        if let date = self.parse(value) {
            let components = Calendar.current.dateComponents(units, from: date)
            return Calendar.current.date(from: components)
        }

        return nil
    }

    /**
     Format date into String representation using specified options
     
     - parameter date:     NSDate object
     
     - returns: String representation of the date
     */
    public static func format(_ date: Date) -> String? {
        let formatter = self.dateFormatter(format: dateTimeFormatString, timeZone: nil)
        return formatter.string(from: date)
    }

    /**
     Format date into String representation using specified options
     
     - parameter date:     NSDate object
     - parameter timeZone: TimeZone used to formatting date
     
     - returns: String representation of the date
     */
    public static func format(_ date: Date, timeZone: TimeZone) -> String? {
        let formatter = self.dateFormatter(format: dateTimeFormatString, timeZone: timeZone)
        return formatter.string(from: date)
    }

    /**
     Format date into String representation using specified options
     
     - parameter date:     NSDate object
     - parameter format:   Format of output value
     
     - returns: String representation of the date
     */
    public static func format(_ date: Date, format: String) -> String? {
        let formatter = self.dateFormatter(format: format, timeZone: nil)
        return formatter.string(from: date)
    }

    /**
     Format date into String representation using specified options
     
     - parameter date:     NSDate object
     - parameter format:   Format of output value
     - parameter timeZone: TimeZone used to formatting date
     
     - returns: String representation of the date
     */
    public static func format(_ date: Date, format: String, timeZone: TimeZone) -> String? {
        let formatter = self.dateFormatter(format: format, timeZone: timeZone)
        return formatter.string(from: date)
    }

    /**
     Check date equality by specified units
     
     - parameter date1: First date
     - parameter date2: Second date
     - parameter units: Units to compare in dates
     
     - returns: Return true if dates are compared by specified NSCalendarUnit's
     */
    public static func isDate(_ date1: Date, equalToDate date2: Date, toGranularity component: Calendar.Component) -> Bool {
        if date1 == date2 {
            return true
        }

        let calendar = Calendar.current

        if #available(iOS 8.0, *) {
            return calendar.isDate(date1, equalTo: date2, toGranularity: component)
        } else {
            // Fallback on earlier versions
            return self._isDate(date1, equalToDate: date2, components: [component])
        }
    }

    public static func isDate(_ date1: Date, equalToDate date2: Date, components: Set<Calendar.Component>) -> Bool {
        return self._isDate(date1, equalToDate: date2, components: components)
    }

    fileprivate static func _isDate(_ date1: Date, equalToDate date2: Date, components: Set<Calendar.Component>) -> Bool {
        let calendar = Calendar.current

        let comps1 = calendar.dateComponents(components, from: date1)
        let comps2 = calendar.dateComponents(components, from: date2)

        return (comps1 == comps2)
    }
}
