//
//  BinaryUtils.h
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 21.03.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#ifndef _BinaryUtils_h
#define _BinaryUtils_h


#define SET_BIT(bit, bitmask) (bitmask = (bitmask | bit))
#define RESET_BIT(bit, bitmask) (bitmask &= (~bit))

#define IS_BIT_SET(bit, bitmask) ((bitmask & bit) == bit)

#endif
