/***********************************************************************\
 * Copyright (c) 2015
 *                                                                       *
 * Binary conversions
\***********************************************************************/


#ifndef BINARY_CONVERSIONS_H
#define BINARY_CONVERSIONS_H


#include <stdint.h>


/*************************************************************************
*  Format conversion functions
*************************************************************************/

uint8_t htoc (uint8_t c);
uint8_t hex2char (const uint8_t* src_buf);

void char2hex (uint8_t c, uint8_t* dst_buf);

void short2hex (const uint8_t* src_buf, uint8_t* dst_buf);
void hex2short (const uint8_t* src_buf, uint8_t* dst_buf);

void long2hex (const uint8_t* src_buf, uint8_t* dst_buf);
void hex2long (const uint8_t* src_buf, uint8_t* dst_buf);

void bin2hex (const uint8_t* src_buf, uint8_t* dst_buf, uint32_t len);
void hex2bin (const uint8_t* src_buf, uint8_t* dst_buf, uint32_t len);

uint32_t is_hex_digit (uint8_t c);

uint8_t float_to_bcd(float val);
float bcd_to_float(uint8_t bcd);

/* ==================================================================== */


/*************************************************************************
*  Print buffer functions
*************************************************************************/

void print_buf(const uint8_t* buf, uint32_t len);
void print_chars(const uint8_t* buf, uint32_t len);
void print_rx_buff(const uint8_t* buf, uint32_t len);

/* ==================================================================== */


#endif // BINARY_CONVERSIONS_H
