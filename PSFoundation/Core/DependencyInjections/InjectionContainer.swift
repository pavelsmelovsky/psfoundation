//
//  InjectionContainer.swift
//  Merck
//
//  Created by Pavel Smelovsky on 03.11.2017.
//  Copyright © 2017 Onza.Me. All rights reserved.
//

import Foundation

public protocol InjectionResolver {
    func resolve(_ object: Any?)
}

public protocol InjectionContainer: InjectionResolver {
    func register<T>(_ block: @escaping (inout T) -> Swift.Void)
}
