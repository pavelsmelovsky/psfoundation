//
//  Configurator.swift
//  Merck
//
//  Created by Pavel Smelovsky on 03.11.2017.
//  Copyright © 2017 Onza.Me. All rights reserved.
//

import Foundation

public protocol Configurator {
    func create() -> InjectionContainer
}
