//
//  Storyboard+DependencyInjection.swift
//  PSFoundation
//
//  Created by Pavel Smelovsky on 08.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation
import ObjectiveC

#if os(macOS)
import AppKit

typealias AppStoryboard = NSStoryboard
#elseif os(iOS)
import UIKit

typealias AppStoryboard = UIStoryboard
#endif

private let kInjectionResolverProperty = malloc(4)!

extension AppStoryboard: InjectionResolverDependency {
    @nonobjc public var injectionResolver: InjectionResolver! {
        get {
            return objc_getAssociatedObject(self, kInjectionResolverProperty) as? InjectionResolver
        }
        set {
            objc_setAssociatedObject(self, kInjectionResolverProperty, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    @objc public func resolveDependency(for object: Any?) {
        if let injectionResolver = self.injectionResolver {
            injectionResolver.resolve(object)
        }
    }
}
