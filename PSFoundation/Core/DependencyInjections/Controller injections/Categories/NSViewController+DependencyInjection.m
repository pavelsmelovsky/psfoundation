//
//  NSViewController+DependencyInjection.m
//  PSFoundation
//
//  Created by Pavel Smelovsky on 08.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import "NSViewController+DependencyInjection.h"
#import <objc/runtime.h>
#import "Swizzling.h"

//#import "Storyboard+DependencyInjection.swift"
#import <PSFoundation/PSFoundation-Swift.h>

@implementation ViewControllerClass (DependencyInjection)

+ (void)load
{
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        SwizzleMethod(self, @selector(viewDidLoad), @selector(di_viewDidLoad));
        SwizzleMethod(self, @selector(prepareForSegue:sender:), @selector(di_prepareForSegue:sender:));
    });
}

- (void)di_viewDidLoad {
    [self.storyboard resolveDependencyFor:self];

    [self di_viewDidLoad];
}

- (void)di_prepareForSegue:(StoryboardSegue *)segue sender:(id)sender {
    [self di_resolveDependenciesFor:segue.StoryboardSegueDestinationControllerProperty];

    [self di_prepareForSegue:segue sender:sender];
}

@end
