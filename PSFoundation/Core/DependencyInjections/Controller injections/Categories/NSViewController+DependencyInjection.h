//
//  NSViewController+DependencyInjection.h
//  PSFoundation
//
//  Created by Pavel Smelovsky on 08.04.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_OSX
#import <Cocoa/Cocoa.h>
#define ViewControllerClass NSViewController
#define StoryboardSegue NSStoryboardSegue
#define StoryboardSegueDestinationControllerProperty destinationController
#elif TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#define ViewControllerClass UIViewController
#define StoryboardSegue UIStoryboardSegue
#define StoryboardSegueDestinationControllerProperty destinationViewController
#endif

@interface ViewControllerClass (DependencyInjection)

@end
