//
//  UITabBarController+DependencyInjection.m
//  PSFoundation
//
//  Created by Pavel Smelovsky on 11.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE

#import <UIKit/UIKit.h>

#import <PSFoundation/PSFoundation-Swift.h>
#import "UITabBarController+DependencyInjection.h"
#import "Swizzling.h"

@implementation UITabBarController (DependencyInjection)

+ (void)load
{
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        SwizzleMethod(self, @selector(viewDidLoad), @selector(di_tabbarctrl_viewDidLoad));
    });
}

- (void)di_tabbarctrl_viewDidLoad {
    for (UIViewController *viewController in self.viewControllers) {
        [self di_resolveDependenciesFor:viewController];
    }

    [self di_tabbarctrl_viewDidLoad];
}

@end

#endif
