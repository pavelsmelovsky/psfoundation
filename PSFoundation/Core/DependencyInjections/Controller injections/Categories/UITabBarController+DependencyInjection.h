//
//  UITabBarController+DependencyInjection.h
//  PSFoundation
//
//  Created by Pavel Smelovsky on 11.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE

#import <UIKit/UIKit.h>

@interface UITabBarController (DependencyInjection)

@end

#endif
