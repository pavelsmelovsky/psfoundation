//
//  NSViewController+ResolveSegue.swift
//  PSFoundation
//
//  Created by Pavel Smelovsky on 11.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//
import Foundation

#if os(macOS)
import AppKit

typealias DIViewController = NSViewController
#elseif os(iOS)
import UIKit

typealias DIViewController = UIViewController
#endif

private let kInjectionResolverProperty = malloc(4)!

// MARK: - Implementation

extension DIViewController: InjectionResolverDependency {
    @nonobjc public var injectionResolver: InjectionResolver! {
        get {
            return objc_getAssociatedObject(self, kInjectionResolverProperty) as? InjectionResolver
        }
        set {
            objc_setAssociatedObject(self, kInjectionResolverProperty, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    @objc(di_resolveDependenciesFor:)
    public func resolveDependencies(for object: Any?) {
        if let injectionResolver = injectionResolver {
            injectionResolver.resolve(object)
        }
    }
}
