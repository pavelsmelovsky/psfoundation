//
//  InjectionResolverDependency.swift
//  Merck
//
//  Created by Pavel Smelovsky on 10.11.2017.
//  Copyright © 2017 Onza.Me. All rights reserved.
//

import Foundation

public protocol InjectionResolverDependency {
    var injectionResolver: InjectionResolver! { get set }
}
