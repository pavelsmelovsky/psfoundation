//
//  DefaultInjectionContainer.swift
//  Merck
//
//  Created by Pavel Smelovsky on 03.11.2017.
//  Copyright © 2017 Onza.Me. All rights reserved.
//

import Foundation

public class DefaultInjectionContainer: InjectionContainer {

    private typealias InjectionCallback = (Any) -> Swift.Void

    private var callbacks: [InjectionCallback]

    public init() {
        self.callbacks = [InjectionCallback]()
    }

    public func resolve(_ object: Any?) {
        if let object = object {
            callbacks.forEach({$0(object)})
        }
    }

    public func register<T>(_ block: @escaping (inout T) -> Void) {
        let callback = { (object: Any) in
            if var object = object as? T {
                block(&object)
            }
        }

        self.callbacks.append(callback)
    }
}
