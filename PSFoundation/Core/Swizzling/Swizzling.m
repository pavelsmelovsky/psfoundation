//
//  Swizzling.m
//  PSFoundation
//
//  Created by Pavel Smelovsky on 11.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

void SwizzleMethod(Class c, SEL original, SEL alternate) {
    Method origMethod = class_getInstanceMethod(c, original);
    Method newMethod = class_getInstanceMethod(c, alternate);

    if (class_addMethod(c, original, method_getImplementation(newMethod), method_getTypeEncoding(newMethod))){
        class_replaceMethod(c, alternate, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, newMethod);
    }
}
