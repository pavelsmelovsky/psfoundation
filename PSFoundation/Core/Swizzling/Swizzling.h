//
//  Swizzling.h
//  PSFoundation
//
//  Created by Pavel Smelovsky on 11.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#ifndef Swizzling_h
#define Swizzling_h

#import <objc/runtime.h>

void SwizzleMethod(Class c, SEL original, SEL alternate);

#endif /* Swizzling_h */
