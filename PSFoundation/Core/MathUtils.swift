//
//  MathUtils.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 22.12.14.
//  Copyright (c) 2014 Pavel Smelovsky. All rights reserved.
//

import Foundation
import CoreGraphics

public func deg2rad(angle: Double) -> Double {
    return angle / (180.0) * Double.pi
}

public func deg2rad(angle: Float) -> Float {
    return angle / (180.0) * Float.pi
}

public func deg2rad(angle: CGFloat) -> CGFloat {
    return angle / 180.0 * CGFloat.pi
}
