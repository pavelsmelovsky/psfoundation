//
//  PSLocationManager.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 18.11.14.
//  Copyright (c) 2014 Pavel Smelovsky. All rights reserved.
//

#if os(iOS)

import UIKit

import CoreLocation

var instance: PSLocationManager?

let kPSDistanceDefault: Double = 2000.0

public enum TrackingState {
    case none
    case tracking
    case suspended
}

public class PSLocationManager: NSObject, CLLocationManagerDelegate {
    private static var _instance: PSLocationManager!

    public class var defaultManager: PSLocationManager {
        if _instance == nil {
            _instance = PSLocationManager()
        }
        return _instance
    }

    private var _state: TrackingState = .none

    public var state: TrackingState {
        return _state
    }

    // stop tracking user location when it firstly founded
    private var stopWhenUpdated: Bool = false

    private var completion: ((_ success: Bool, _ location: CLLocation?) -> Void)?

    private var locationManager: CLLocationManager

    private var currentLocation: CLLocation?

    override init() {
        self.locationManager = CLLocationManager()

        super.init()
        self.locationManager.distanceFilter = kPSDistanceDefault
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.delegate = self

        if #available(iOS 8, *) {
            self.locationManager.requestWhenInUseAuthorization()
        }

        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(PSLocationManager.applicationDidEnterBackgroundNotification(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        center.addObserver(self, selector: #selector(PSLocationManager.applicationWillEnterForegroundNotification(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    @objc public func applicationDidEnterBackgroundNotification(_ notification: NSNotification) {
        self.locationManager.stopUpdatingLocation()
    }

    @objc public func applicationWillEnterForegroundNotification(_ notification: NSNotification) {
        self.locationManager.startUpdatingLocation()
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.last

        if self.stopWhenUpdated {
            self.locationManager.stopUpdatingLocation()
            self._state = .none

            self.completion?(true, self.currentLocation)
        }

        if let loc = self.currentLocation {
            NSLog("location: %@", loc.description)
        }
    }

    public func startUpdatingLocation() {
        if self._state == .none {
            self.stopWhenUpdated = false

            self._state = .tracking
            self.locationManager.startUpdatingLocation()
        }
    }

    public func stopUpdatingLocation() {
        self._state = .none
        self.locationManager.stopUpdatingLocation()
    }

    public func currentUserLocation(completion: @escaping (_ success: Bool, _ location: CLLocation?) -> Void) {
        if self._state == .tracking || self._state == .suspended {
            completion(true, self.lastKnownLocation())
        } else {
            self.completion = completion

            self.startUpdatingLocation()
            self.stopWhenUpdated = true
        }
    }

    // MARK: public getter methods

    public func lastKnownLocation() -> CLLocation? {
        if let current = self.currentLocation {
            return current
        } else if let location = self.locationManager.location {
            let locationTime = location.timestamp.timeIntervalSinceReferenceDate
            let timeDiff = Date.timeIntervalSinceReferenceDate - locationTime

            // location tracked no earlier then half hour ago
            if timeDiff >= 0 && timeDiff <= 3600 * 0.5 {
                return location
            }
        }
        return nil
    }
}

#endif
