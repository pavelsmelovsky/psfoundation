//
//  String+PathComponents.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 17.09.15.
//  Copyright © 2015 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension String {

    var lastPathComponent: String {
        return NSString(string: self).lastPathComponent
    }

    var pathExtension: String {
        return NSString(string: self).pathExtension
    }

    var stringByDeletingLastPathComponent: String {
        return NSString(string: self).deletingLastPathComponent
    }

    var stringByDeletingPathExtension: String {
        return NSString(string: self).deletingPathExtension
    }

    var pathComponents: [String] {
        return NSString(string: self).pathComponents
    }

    func stringByAppendingPathComponent(_ path: String) -> String {
        return NSString(string: self).appendingPathComponent(path)
    }

    func stringByAppendingPathExtension(_ ext: String) -> String? {
        return NSString(string: self).appendingPathExtension(ext)
    }
}
