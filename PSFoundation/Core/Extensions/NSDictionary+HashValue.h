//
//  NSDictionary+HashValue.h
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 27.06.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (HashValue)

- (NSUInteger)ps_hashValue;

@end
