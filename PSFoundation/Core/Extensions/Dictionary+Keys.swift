//
//  Dictionary+Keys.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 01.03.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension Dictionary {
    static func allKeys<K, V: Equatable>(dict: [K: V], forValue val: V) -> [K] {
        return dict.filter({$1 == val}).map { $0.0 }
    }
}
