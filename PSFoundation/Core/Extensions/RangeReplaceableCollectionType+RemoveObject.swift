//
//  RangeReplaceableCollectionType+RemoveObject.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 14.03.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension RangeReplaceableCollection where Iterator.Element: Equatable {
    mutating func removeObject(object: Iterator.Element) {
        if let index = self.firstIndex(of: object) {
            #if DEBUG
                print("Total count of items: \(self.count)")
                print("Will remove object \(object) at index \(index)")
            #endif

            self.remove(at: index)
        }
    }
}
