//
//  UITableView+DataSource.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 28.01.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#if os(iOS)

import UIKit

public extension UITableView {
    func isEmpty() -> Bool {
        for idx in 0 ..< self.numberOfSections {
            if self.numberOfRows(inSection: idx) > 0 {
                return false
            }
        }

        return true
    }
}

#endif
