//
//  FileManager+App.swift
//  PSFoundation
//
//  Created by Pavel Smelovsky on 06.11.14.
//  Copyright (c) 2014 Smelovsky.Me. All rights reserved.
//

import Foundation

public extension FileManager {
    var applicationDocumentsDirectory: URL {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "me.onza.<ProjectName>" in the application's documents Application Support directory.
        let urls = self.urls(for: .documentDirectory, in: .userDomainMask)
        return urls.last!
    }

    var applicationCacheDirectory: URL {
        let url = try? self.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return url!
    }

}
