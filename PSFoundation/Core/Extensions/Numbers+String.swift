//
//  Numbers=String.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 17.11.14.
//  Copyright (c) 2014 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension Double {
    func toString() -> String {
        return String(format: "%.7f", self)
    }
}
