//
//  Data+Hex.swift
//  PSFoundation
//
//  Created by Pavel Smelovsky on 22.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public extension Data {
    var hex: String {
        var bytes = [UInt8]()
        bytes.reserveCapacity(self.count * 2)

        self.withUnsafeBytes { (pointer) -> Swift.Void in
            if let pointer = pointer.baseAddress?.assumingMemoryBound(to: UInt8.self) {
                bin2hex(srcBuf: pointer, dstBuf: &bytes, len: self.count)
            }
        }

        return String(data: Data(bytes), encoding: .ascii)!
    }
}

let hex = "0123456789ABCDEF"

func char2hex(char: UInt8, dstBuf: inout [UInt8], offset: Int) {
    let char1 = hex.utf8CString[Int(char >> 4)]
    let char2 = hex.utf8CString[Int(char & 0x0F)]
    dstBuf.append(UInt8(char1))
    dstBuf.append(UInt8(char2))
}

func bin2hex (srcBuf: UnsafePointer<UInt8>, dstBuf: inout [UInt8], len: Int) {
    var counter = 0
    var pointer = srcBuf
    repeat {
        char2hex(char: pointer.pointee, dstBuf: &dstBuf, offset: counter * 2)
        pointer = pointer.successor()
        counter += 1
    } while counter < len
}
