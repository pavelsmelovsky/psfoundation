//
//  NSData+HEX.m
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 31.07.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import "NSData+HEX.h"
#import "BinaryConversions.h"

@implementation NSData (HEX)

+ (instancetype)dataFromHex:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    if (data == nil) {
        return nil;
    }

    uint8_t *dstBuf = malloc(data.length / 2);
    hex2bin(data.bytes, dstBuf, (uint32_t)data.length);
    return [NSData dataWithBytesNoCopy:dstBuf length:data.length / 2 freeWhenDone:YES];
}

- (NSString *)hexString
{
    uint8_t* dst_buf = malloc(self.length * 2);
    bin2hex(self.bytes, dst_buf, (uint32_t)self.length);
    
    NSData* data = [NSData dataWithBytesNoCopy:dst_buf length:self.length * 2 freeWhenDone:YES];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

@end
