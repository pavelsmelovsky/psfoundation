//
//  Thread+Utils.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 13.06.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension Thread {
    class func executeOnMainThread(block: @escaping () -> Void) {
        if Thread.isMainThread {
            block()
        } else {
            DispatchQueue.main.async(execute: block)
        }
    }
}
