//
//  NSData+HEX.h
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 31.07.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (HEX)

+ (instancetype)dataFromHex:(NSString *)string;

- (NSString *)hexString;

@end
