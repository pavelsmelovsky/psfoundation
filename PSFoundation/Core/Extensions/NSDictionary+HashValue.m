//
//  NSDictionary+HashValue.m
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 27.06.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

#import "NSDictionary+HashValue.h"

@implementation NSDictionary (HashValue)

- (NSUInteger)ps_hashValue
{
    __block NSUInteger hash = 0;
    
    [self enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        hash ^= [key hash] ^ [obj hash];
    }];
    
    return hash;
}

@end
