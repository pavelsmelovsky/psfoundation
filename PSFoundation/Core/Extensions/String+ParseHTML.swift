//
//  String+ParseHTML.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 27.06.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension String {
    func cutHTMLTags() -> String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
    }

    func parseHTML() -> NSMutableAttributedString {
        return self.parseHTML(attributes: nil)
    }

    func parseHTML(attributes: [NSAttributedString.Key: Any]?) -> NSMutableAttributedString {
        var attrText: NSMutableAttributedString!

        if let textData = self.data(using: String.Encoding.utf8) {
            let options: [NSAttributedString.DocumentReadingOptionKey: Any] =
                [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                 NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]
            do {
                attrText = try NSMutableAttributedString(data: textData, options: options, documentAttributes: nil)
            } catch {
                attrText = NSMutableAttributedString(string: self.cutHTMLTags())
            }
        } else {
            attrText = NSMutableAttributedString(string: self.cutHTMLTags())
        }

        if let attrs = attributes {
            let fullRange = NSRange(location: 0, length: attrText.length)
            attrText.addAttributes(attrs, range: fullRange)
        }

        return attrText
    }

    func parseHTMLAndCache() -> NSMutableAttributedString {
        return parseHTMLAndCache(attributes: nil)
    }

    func parseHTMLAndCache(attributes: [NSAttributedString.Key: Any]?) -> NSMutableAttributedString {
        let cache = StringCache.cache()

        let cacheStringInfo = CachedStringInfo(value: self, attributes: attributes)

        if let value = cache.object(forKey: cacheStringInfo) as? NSMutableAttributedString {
            return value
        }

        let text = parseHTML(attributes: attributes)

        cache.setObject(text, forKey: cacheStringInfo)

        return text
    }
}

private class CachedStringInfo: NSObject {
    var string: String
    var attributes: [NSAttributedString.Key: Any]?

    init(value: String, attributes: [NSAttributedString.Key: Any]?) {
        self.string = value
        self.attributes = attributes

        super.init()
    }

    override var hash: Int {
        var aHash: Int = 0
        if let attrs = self.attributes {
            attrs.forEach({ (item) in
                aHash ^= item.0.hashValue

                if let hashable = item.1 as? NSObject {
                    aHash ^= hashable.hashValue
                }
            })
        }
        return self.string.hashValue ^ aHash
    }
}

private func == (lhs: CachedStringInfo, rhs: CachedStringInfo) -> Bool {
    if lhs === rhs {
        return true
    }

    return lhs.hash == rhs.hash
}

private class StringCache: NSCache<CachedStringInfo, NSAttributedString> {
    private static var _instance: StringCache?

    static func cache() -> StringCache {
        if _instance == nil {
            _instance = StringCache()
        }
        return _instance!
    }
}
