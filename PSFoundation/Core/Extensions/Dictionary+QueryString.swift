//
//  Dictionary_QueryString.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 06.03.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension Dictionary where Key == String, Value == CustomStringConvertible {
    func buildQueryString() -> String? {
        if self.isEmpty {
            return nil
        }

        var paramArray = [String]()

        for item in self {
            paramArray.append("\(item.key)=\(item.value.description)")
        }

        return paramArray.joined(separator: "&")
    }

    static func parse(queryString: String) -> [String: Any] {
        var dictionary = [String: Any]()

        let separator = CharacterSet(charactersIn: "=&")
        let keyvalues = queryString.components(separatedBy: separator)

        guard keyvalues.count % 2 == 0 else {
            return dictionary
        }

        var iterator = keyvalues.makeIterator()

        while let key = iterator.next(), let value = iterator.next() {
            dictionary[key] = value
        }

        return dictionary
    }
}
