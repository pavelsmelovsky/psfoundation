//
//  NSCalendar_DatesExt.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 13.07.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension Calendar {
    func allComponentUnits() -> Set<Calendar.Component> {
        let allComponents: [Calendar.Component] = [.era, .year, .month, .day, .hour, .minute, .second, .weekday, .weekdayOrdinal, .quarter, .weekOfMonth, .weekOfYear, .yearForWeekOfYear, .nanosecond, .calendar, .timeZone]
        return Set<Calendar.Component>(allComponents)
    }

    func allComponentUnitsExcept(_ except: Set<Calendar.Component>) -> Set<Calendar.Component> {
        let units = self.allComponentUnits()
        return units.subtracting(except)
    }

    func number(ofDaysInMonth month: Int, forDate date: Date) -> Int {
        // prepare components for received date
        var components = self.dateComponents(self.allComponentUnits(), from: date)
        // update month
        components.month = month

        // generate new nsDate object with updated components
        guard let monthDate = self.date(from: components) else {
            return 0
        }

        // get range of days in month
        guard let range = self.range(of: .day, in: .month, for: monthDate) else {
            return 0
        }
        // return number of days
        return range.count
    }

    func number(ofDaysInMonthForDate date: Date) -> Int {
        guard let range = self.range(of: .day, in: .month, for: date) else {
            return 0
        }
        return range.count
    }

    func start(of period: Calendar.Component, forDate date: Date) -> Date? {
        if period == .year {
            return self.start(ofYearForDate: date)
        } else if period == .month {
            return self.start(ofMonthForDate: date)
        }

        return nil
    }

    func start(ofYearForDate date: Date) -> Date! {
        let units: Set<Calendar.Component> = [.era, .year]
        var components = self.dateComponents(units, from: date)

        components.month = 1
        components.day = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        components.nanosecond = 0
        components.timeZone = TimeZone.autoupdatingCurrent

        return self.date(from: components)
    }

    func start(ofMonthForDate date: Date) -> Date! {
        let units: Set<Calendar.Component> = [.era, .year, .month]
        var components = self.dateComponents(units, from: date)

        components.day = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        components.nanosecond = 0
        components.timeZone = TimeZone.autoupdatingCurrent

        return self.date(from: components)
    }

    func end(of period: Calendar.Component, forDate date: Date) -> Date? {
        if period == .year {
            return self.end(ofYearForDate: date)
        } else if period == .month {
            return self.end(ofMonthForDate: date)
        }

        return nil
    }

    func end(ofYearForDate date: Date) -> Date! {
        let units: Set<Calendar.Component> = [.era, .year]
        var components = self.dateComponents(units, from: date)

        let numberOfDays = self.number(ofDaysInMonth: 12, forDate: date)

        components.month = self.monthSymbols.count
        components.day = numberOfDays
        components.hour = 23
        components.minute = 59
        components.second = 59
        components.nanosecond = Int(NSEC_PER_SEC)
        //components.timeZone = NSTimeZone.localTimeZone();

        return self.date(from: components)
    }

    func end(ofMonthForDate date: Date) -> Date! {
        let units: Set<Calendar.Component> = [.era, .year, .month]
        var components = self.dateComponents(units, from: date)

        components.day = self.number(ofDaysInMonthForDate: date)
        components.hour = 23
        components.minute = 59
        components.second = 59
        components.nanosecond = Int(NSEC_PER_SEC)
        //components.timeZone = NSTimeZone.localTimeZone();

        return self.date(from: components)
    }

    func ps_endOfDayForDate(_ date: Date, ignoreNanoseconds: Bool = false) -> Date {
        return self._endOfDayForDate(date, ignoreNanoseconds: ignoreNanoseconds)
    }

    fileprivate func _startOfDayForDate(_ date: Date) -> Date {
        let units: Set<Calendar.Component> = [.era, .year, .month, .day]
        var components = self.dateComponents(units, from: date)

        components.hour = 0
        components.minute = 0
        components.second = 0
        components.nanosecond = 0

        return self.date(from: components)!
    }

    fileprivate func _endOfDayForDate(_ date: Date, ignoreNanoseconds: Bool = false) -> Date {
        let units: Set<Calendar.Component> = [.era, .year, .month, .day]
        var components = self.dateComponents(units, from: date)

        components.hour = 23
        components.minute = 59
        components.second = 59
        components.nanosecond = ignoreNanoseconds ? 0 : Int(NSEC_PER_SEC)

        return self.date(from: components)!
    }

    func yesterdayFromDate(_ date: Date) -> Date? {
        return self.date(byAdding: .day, value: -1, to: date)
    }
}
