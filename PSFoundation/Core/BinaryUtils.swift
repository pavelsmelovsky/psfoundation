//
//  BinaryUtils.swift
//  Pavel Smelovsky
//
//  Created by Pavel Smelovsky on 07.03.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import Foundation

public func INT_BIT() -> Int {
    return Int(CHAR_BIT) * MemoryLayout<Int>.size
}

// rotate bits
public func rotateBits(_ value: Int, repeatTimes: Int) -> Int {
    return (value << repeatTimes) | (value >> (INT_BIT() - repeatTimes))
}
