/**
 *
 *
 * Implementation of HAL utility functions
 *
 */

#include "BinaryConversions.h"

/*************************************************************************
*  Format conversion functions
*************************************************************************/

static uint8_t hex[] = "0123456789ABCDEF";

uint8_t htoc (uint8_t c)
{
    return (c <= '9') ? (c - '0') : ((c | 0x20) - 'a' + 10);
}

uint8_t hex2char (const uint8_t* src_buf)
{
    return (htoc(src_buf[0]) << 4) + htoc(src_buf[1]);
}

void char2hex (uint8_t c, uint8_t* dst_buf)
{
    dst_buf[0] = hex[c >> 4];
    dst_buf[1] = hex[c & 0x0F];
}

void short2hex (const uint8_t* src_buf, uint8_t* dst_buf)
{
    char2hex(src_buf[0], &dst_buf[0]);
    char2hex(src_buf[1], &dst_buf[2]);
}

void hex2short (const uint8_t* src_buf, uint8_t* dst_buf)
{
    dst_buf[0] = hex2char(&src_buf[0]);
    dst_buf[1] = hex2char(&src_buf[2]);
}

void long2hex (const uint8_t* src_buf, uint8_t* dst_buf)
{
    char2hex(src_buf[0], &dst_buf[0]);
    char2hex(src_buf[1], &dst_buf[2]);
    char2hex(src_buf[2], &dst_buf[4]);
    char2hex(src_buf[3], &dst_buf[6]);
}

void hex2long (const uint8_t* src_buf, uint8_t* dst_buf)
{
    dst_buf[0] = hex2char(&src_buf[0]);
    dst_buf[1] = hex2char(&src_buf[2]);
    dst_buf[2] = hex2char(&src_buf[4]);
    dst_buf[3] = hex2char(&src_buf[6]);
}

void bin2hex (const uint8_t* src_buf, uint8_t* dst_buf, uint32_t len)
{
    for (uint32_t i = 0; i < len; i++)
    {
        char2hex(src_buf[i], &dst_buf[i * 2]);
    }
}

void hex2bin (const uint8_t* src_buf, uint8_t* dst_buf, uint32_t len)
{
    uint32_t i;

    for (i = 0; i < len / 2; i++) {
        dst_buf[i] = hex2char(&src_buf[2 * i]);
    }
}

uint32_t is_hex_digit (uint8_t c)
{
    if (((c >= '0') && (c <= '9')) ||
        ((c >= 'A') && (c <= 'F')) ||
        ((c >= 'a') && (c <= 'f')))
    {
        return 1;
    }
    return 0;
}

uint8_t float_to_bcd(float val)
{
    if (val < 0) {
        return 0xFF;
    } else if (val > 15.9) {
        return 0xFE;
    }

    return (((((int)val) & 0x0F) << 4) | (((int)((val - (int)val) * 10) & 0x0F)));
}

float bcd_to_float(uint8_t bcd) {
	return ((bcd & 0xf0) >> 4) + (bcd & 0x0f)/10.0f;
}

/* =============================================================== */


/* ******************************************************************
 * Print buffer functions
 * *****************************************************************/

void print_buf(const uint8_t* buf, uint32_t len)
{
    uint32_t i;

    for (i = 0; i < len; i++) {
        //printf("%02X ", buf[i]);
		//HBPRINT(_HBSTR("%02X"), buf[i]);
    }
	
	//HBPRINT(_HBSTR("\n\n"));
}

void print_chars(const uint8_t* buf, uint32_t len)
{
    uint32_t i;

    for (i = 0; i < len; i++) {
        //HBPRINT(_HBSTR("%c"), buf[i]);
    }
    //HBPRINT("\n");
}

void print_rx_buff(const uint8_t* buf, uint32_t len)
{
    //HBPRINT(_HBSTR("    RX buffer [%2ld bytes]: "), len);
    print_chars(buf, len);
    //HBPRINT(_HBSTR("\n"));
}
/* =============================================================== */
