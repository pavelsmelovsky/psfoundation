//
//  PSFoundation.h
//  PSFoundation
//
//  Created by Pavel Smelovsky on 19.10.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PSFoundation.
FOUNDATION_EXPORT double PSFoundationVersionNumber;

//! Project version string for PSFoundation.
FOUNDATION_EXPORT const unsigned char PSFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PSFoundation/PublicHeader.h>


